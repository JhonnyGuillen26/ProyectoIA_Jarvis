from flask import Flask, render_template, Response, request, redirect, url_for
import pickle
import matplotlib.pyplot as plt 
import seaborn as sns
import speech_recognition as sr
import webbrowser as wb

class Jervis():

    def __init__(self):
        self.app = Flask(__name__)
        self.phrase = " "
    """
        Mostrar grafico de x=fecha, y=precio
    """
    def getKeyWordModeloAutomoviles(self):
        modelo2 = "Automoviles"
        texto_input = "Prediga el precio de un automovil"
        list_key_word = ["auto","carro","automovil"]

        split_text = texto_input.split()
        
        for word in split_text:

            for key in list_key_word:

                if word == key:

                    self.modeloAutomoviles()
    
    def modeloAutomoviles(self):
        print("Modelo2")
        #plt.figure(figsize=(8,5))     
        #sns.scatterplot(x=1, y='selling_price', data=modelo_automovil)


    """
        Bitcoin
        Que ano?
        Que mes y dia?
    """
    def modeloBitcoin(self):
        decision_ano = "2016"
        decision_mes = "04"
        decision_dia = "01"

        decision_fecha = decision_ano+"-"+decision_mes+"-"+decision_dia

        #Call function modelo to change prediction
        self.prediccionBitcoin(decision_fecha)

    def prediccionBitcoin(self):
        print("..")


    """
        Se puede cambiar el ano.
        Mostrar grafico de x=ano, y=precio
    """
    def getKeyWordModelo1(self):
        texto_input = "Prediga el precio de los aguacates dentro de un año"
        list_key_word = ["aguacates","aguacate"]
        modelo1 = "Aguacates"

        split_text = texto_input.split()
        
        for word in split_text:

            for key in list_key_word:

                if word == key:

                    self.modeloAguacate()

    def modeloAguacate(self):
        print("Modelo1")

    """
        Se pregunta directamente si o no.
            estado civil.
            ciudad.
            estado.
            profesion.
            Dueno de casa.
            Dueno de carro.
    """
    def getKeyWordModelo3(self):
        texto_input = "Predecir si a un cliente se le va dar prestamo."
        list_key_word = ["cliente","prestamo"]
        modelo2 = "Prestamo"

        split_text = texto_input.split()
        
        for word in split_text:

            for key in list_key_word:

                if word == key:

                    self.modeloPrestamo()

    def modeloPrestamo(self):
        print("Modelo3")

    """

    """
    def fn_speech_recognition(self):
        sr.Microphone(device_index = 0)
        print(f"MICs Found on this Computer: \n {sr.Microphone.list_microphone_names()}")
        # Creating a recognition object
        r = sr.Recognizer()
        r.energy_threshold=4000
        r.dynamic_energy_threshold = False

        with sr.Microphone() as source:
            print('Please Speak Loud and Clear:')
            #reduce noise
            r.adjust_for_ambient_noise(source)
            #take voice input from the microphone
            audio = r.listen(source)
            try:
                self.phrase = r.recognize_google(audio)
                print(f"Did you just say: {self.phrase} ?")
                
            except TimeoutException as msg:
                print(msg)
            except WaitTimeoutError:
                print("listening timed out while waiting for phrase to start")
                quit()
            # speech is unintelligible
            except LookupError:
                print("Could not understand what you've requested.")
            else:
                print("Your results will appear in the default browser. Good bye for now...")

    """

    """
    @app.route("/")
    def home(self):
        
        self.getKeyWordModeloAutomoviles()

        param = {'name':"Jeremy"}

        #fn_speech_recognition()

        return render_template("interfaz.html", **param)


    @app.route("/", methods=['POST'])
    def move_forward(self):

        param = {'name':"Jeremy"}

        self.fn_speech_recognition()
        
        return render_template('interfaz.html', forward_message=phrase, **param);
#
# Main
#
if __name__ == "__main__":
    jervis = Jervis()
    jervis.app.run()



#python3 -m flask run
"""

    #cargar modelo
    #filename = "MoldeloPredecirPrecioAutomovil.sav"
    #modelo_automovil = pickle.load(open(filename, 'rb'))





@app.route("/next")
def next():
    return "Next"


"""